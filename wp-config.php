<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'wordpress-test');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '1111');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'K%:/E)^bO_T_%788@^/2rhD 4@LtcZflk&y[)Z9+{EnEUq?@%YGPz}M(VX|&6EY`');
define('SECURE_AUTH_KEY',  '/[~+v#~&4qe(!~^lx9z%BAhh/LoTkhB-9*1)^{2gZj u_6|AfWP]YW5b^VCop=`&');
define('LOGGED_IN_KEY',    '.BCby*[Wf3ac]$r9In?:5g{5]!!@e|]|J`-:ck}r}zArq5I5x^C=3xrw@1B?U@6h');
define('NONCE_KEY',        'JPoWR)#oz40t.>Dw{u:/mMytUFljZKvSjV_kjN/[31&Gr*a6]{lok> _Ffd(*j-T');
define('AUTH_SALT',        'NmABw(Y~41_7fc? R|Ei#QI#3Y;N[$P+5NY|kA<=jU$|(B.eXU7yxK#W2OVTEI`v');
define('SECURE_AUTH_SALT', '}v]9m^Gwqf*KW:(TtY*#p>9g#Aa@akC9v{`~V3R: |nd?wy6j-r%dHULw{#q#0K[');
define('LOGGED_IN_SALT',   'TgJ@t0+Ty-Us<n-P7uXRUs0]4W#e7/#=z99`pr w0B^^u0@q$)#7C7A9G`O{KzPX');
define('NONCE_SALT',       'sxJ3FThd_vJ?.HkT*C(dr6P]PNf@vmt./1SKQ|!p2,lxn$:xPvBd[ Eah6(P:4HU');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

define('FS_METHOD', 'direct');

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
